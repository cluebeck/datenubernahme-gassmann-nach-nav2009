// Für das Testsystem
SET ANSI_NULLS ON
GO

SET ANSI_WARNINGS ON
GO

SELECT *
FROM [GASSMANNTEST].[gass2018tst].[dbo].[Company]
GO


// Für das Produktivsystem
SET ANSI_NULLS ON
GO

SET ANSI_WARNINGS ON
GO

SELECT *
FROM [GASSMANN].[lerbs365coreprd].[dbo].[Company]
GO

