EXEC sp_addlinkedserver
@server='GASSMANNTEST', -- here you can specify the name of the linked server
@srvproduct='',
@provider='sqlncli',
@datasrc='lerbssqlcore.database.windows.net', -- @datasrc='lerbssqlcore.database.windows.net',
@location='',
@provstr='',
@catalog='gass2018tst'
GO

EXEC sp_addlinkedsrvlogin @rmtsrvname=N'GASSMANNTEST',
@useself=N'False',
@locallogin=NULL,
@rmtuser=N'lerbs.cluebeck@lerbssqlcore.database.windows.net',
@rmtpassword='xxxxxxxxxxxxxxxx'
GO

EXEC sp_serveroption 'GASSMANNTEST', 'rpc out', true;
GO

 