EXEC sp_addlinkedserver
@server='GASSMANN', -- here you can specify the name of the linked server
@srvproduct='',
@provider='sqlncli',
@datasrc='lerbssqlcore.database.windows.net', -- @datasrc='lerbssqlcore.database.windows.net',
@location='',
@provstr='',
@catalog='lerbs365coreprd'
GO

EXEC sp_addlinkedsrvlogin @rmtsrvname=N'GASSMANN',
@useself=N'False',
@locallogin=NULL,
@rmtuser=N'lerbs.cluebeck@lerbssqlcore.database.windows.net',
@rmtpassword='xxxxxxxxxxxxx'
GO

EXEC sp_serveroption 'GASSMANN', 'rpc out', true;
GO

 