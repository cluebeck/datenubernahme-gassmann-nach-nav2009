EXEC sp_addlinkedserver
@server='LERBSTEST', -- here you can specify the name of the linked server
@srvproduct='',
@provider='sqlncli',
@datasrc='lerbssqlcore.database.windows.net', -- @datasrc='lerbssqlcore.database.windows.net',
@location='',
@provstr='',
@catalog='lerbs365coretst'
GO


EXEC sp_addlinkedsrvlogin @rmtsrvname=N'LERBSTEST',
@useself=N'False',
@locallogin=NULL,
@rmtuser=N'lerbs.cluebeck@lerbssqlcore.database.windows.net',
@rmtpassword='xxxxxxxxxxxxxx'
GO



EXEC sp_serveroption 'LERBSTEST', 'rpc out', true;
GO
