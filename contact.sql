USE [Lerbs2009Test072018]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Lerbs AG$NAV18 Contact]
AS SELECT
	[timestamp],
	[No_],
	[Name],
	[Search Name],
	[Name 2],
	[Address],
	[Address 2],
	[City],
	[Phone No_],
	[Telex No_],
	[Territory Code],
	[Currency Code],
	[Language Code],
	[Salesperson Code],
	[Country_Region Code],
	[Last Date Modified],
	[Fax No_],
	[Telex Answer Back],
	[VAT Registration No_],
	[Post Code],
	[County],
	[E-Mail],
	[Home Page],
	[No_ Series],
	[Privacy Blocked],
	[Minor],
	[Parental Consent Received],
	[Type],
	[Company No_],
	[Company Name],
	[Lookup Contact No_],
	[First Name],
	[Middle Name],
	[Surname],
	[Job Title],
	[Initials],
	[Extension No_],
	[Mobile Phone No_],
	[Pager],
	[Organizational Level Code],
	[Exclude from Segment],
	[External ID],
	[Correspondence Type],
	[Salutation Code],
	[Search E-Mail],
	[Last Time Modified],
	[E-Mail 2],
	[Xrm Id],
	[Department],
	[Tranfer NAv2009],
	[Old Contact No_],
	[Responsibility Center],
	[Birth Date],
	[Sex],
	[Salesperson Yes_No],
	[Last Entry No_],
	[Customer Type],
	[Customer No_],
	[Vendor No_],
	[Search Name internal] 
FROM [GASSMANNTEST].[gass2018tst].[dbo].[Zentralmandant$Contact]

GO


